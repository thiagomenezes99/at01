import { Component, OnInit } from '@angular/core';
import { Item } from '../home/Item';
import {Router, ActivatedRoute } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.page.html',
  styleUrls: ['./produto.page.scss'],
})
export class ProdutoPage {
  public productForm: FormGroup;
  public item: Item;
  public atualizar: number;

  constructor(public router: Router, public formBuilder: FormBuilder, private route: ActivatedRoute) { 
    this.productForm = this.formBuilder.group({
      productName: ['', Validators.required],
      productDesc: ['', Validators.required],
      productCategory: ['', Validators.required]
    });

    this.route.queryParams.subscribe(params => {
      if (params && params.item && params.indiceStorage) {
        this.item = JSON.parse(params.item);
        this.atualizar = params.indiceStorage;

        this.productForm.controls['productName'].setValue(this.item.nome);
        this.productForm.controls['productDesc'].setValue(this.item.descricao);
        this.productForm.controls['productCategory'].setValue(this.item.categoria);
      } else {
        this.item = new Item('','','');
      }
    });
  }

  salvar() {  
    
    if(this._validarFormulario()) {
      console.log("Por favor, verifique e tente novamente.");
      return;
    }

    this._construirItem(this.productForm.value);
    this._salvarLocalStorage(this.atualizar);
    this.router.navigateByUrl('/home');
  }

  _existeItem(camposFormulario) {
    return(this.atualizar);
  }

  __encontrarIndice(camposFormulario){
    console.log(this.productForm.value.productName);
    let lista = JSON.parse(localStorage.getItem("item"));
    
    let res = lista.filter(this.__compararStrings);

    console.log(res[0].nome);
  }

  __compararStrings(lista) {
    console.log("Teste: " + this.productForm.value.productName);
    return lista.nome == this.productForm.value.productName;
  }

  _validarFormulario(){
    return this.productForm.invalid;
  }
 
  _construirItem(camposFormulario) {
    this.item.nome = camposFormulario.productName;
    this.item.categoria = camposFormulario.productCategory;
    this.item.descricao = camposFormulario.productDesc;
  }

  _salvarLocalStorage(indice){
    let listaItens = [];
    let itens = JSON.parse(localStorage.getItem("item"));

    if(localStorage.length > 0){
      itens.forEach(item => {
        listaItens.push(item);
      });
    }

    if(this.atualizar)
      listaItens[this.atualizar] = this.item;
    else
      listaItens.push(this.item);

    localStorage.setItem('item', JSON.stringify(listaItens));
  }
}
