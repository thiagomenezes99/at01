export class Item {
    public nome: string;
    public categoria: string;
    public descricao: string;

    constructor(nome: string, categoria: string, descricao: string) {
        this.nome = nome;
        this.categoria = categoria;
        this.descricao = descricao;
    }

    public setNome(nome: string){
        this.nome = nome;
    }

    public setCategoria(categoria: string){
        this.categoria = categoria;
    }

    public setDescricao(descricao: string){
        this.descricao = descricao;
    }

    public getNome(){
        return this.nome;
    }

    public getCategoria(){
        return this.categoria;
    }

    public getDescricao(){
        return this.descricao;
    }
}