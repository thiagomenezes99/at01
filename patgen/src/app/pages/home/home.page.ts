import { Component } from '@angular/core';
import { Item } from './Item';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  lista = JSON.parse(localStorage.getItem("item"));

  constructor(private router: Router) {

  }

  detalhe(item: Item, indiceStorage) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        item: JSON.stringify(item),
        indiceStorage
      }
    };
    this.router.navigate(['produto'], navigationExtras);
  }

  excluir(indiceStorage) {
    let filtrados = this.lista.filter((value,index) => index !== indiceStorage);
    localStorage.setItem("item", JSON.stringify(filtrados));
    window.location.reload();
  }

}
